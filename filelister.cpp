#include "filelister.h"
#include <QDir>
#include <QDebug>

FileLister::FileLister(QObject *parent)
    : QObject(parent),
      _files( QStringList() ),
      isDesktop_( false )
{
}

QStringList FileLister::files()
{
    return _files;
}

void FileLister::setFiles( QStringList nFiles )
{
    if ( _files != nFiles )
    {
        _files = nFiles;
        emit filesChanged( _files );
    }
}

void FileLister::setDesktop(bool isDesktop)
{
    isDesktop_ = isDesktop;
    refresh();
}

void FileLister::refresh()
{
    QString dirPath;
    if ( isDesktop_ )
        dirPath = "/home/hpham/Desktop/images/thumbs";
    else
        dirPath = "/home/pi/images/thumbs";
    QDir imageDir(dirPath);
    QStringList images = imageDir.entryList(QStringList() << "*.jpg" << "*.JPG", QDir::Files );
    QStringList replaceImages;
    foreach ( QString image, images )
    {
        replaceImages.append( "file://" + dirPath + "/" + image );
    }
    setFiles( replaceImages );
}

QString FileLister::getNonThumbName(const QString &thumbName)
{
    QString ret = thumbName;
    return ret.replace( "/thumbs", "" );
}
