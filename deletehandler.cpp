#include "deletehandler.h"
#include <QFile>

DeleteHandler::DeleteHandler(QObject *parent) : QObject(parent)
{

}

void DeleteHandler::deleteFile( const QString & fileName )
{
    QString checkFile( fileName );
    checkFile = checkFile.replace("file://","");
    QFile mFile( checkFile );


    if ( mFile.exists() )
        mFile.remove();
}

void DeleteHandler::deleteAllFiles()
{

}
