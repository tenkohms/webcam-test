#ifndef FFMPEGWORKER_H
#define FFMPEGWORKER_H

#include <QObject>
#include <QProcess>

class FFMPEGWORKER : public QObject
{
    Q_OBJECT
public:
    FFMPEGWORKER( QObject *parent = nullptr);
    void setParams( const QString & command, const QStringList & args );
signals:
    void finished();

public slots:
    void process();
private:
    QString _command;
    QStringList _args;
};

#endif // FFMPEGWORKER_H
