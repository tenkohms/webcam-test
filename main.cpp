#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "deletehandler.h"
#include "filelister.h"
#include "ffmpegger.h"
#include "btprinter.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType< DeleteHandler >("Millipore", 1, 0, "Deleter" );
    qmlRegisterType< FileLister >("Millipore", 1, 0, "FLister" );
    qmlRegisterType< FFMPEGGER >("Millipore", 1, 0, "FFMPEG" );
    qmlRegisterType< BTPrinter >("Millipore", 1, 0, "BTPrinter" );

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
