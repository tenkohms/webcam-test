#include "ffmpegworker.h"

FFMPEGWORKER::FFMPEGWORKER(QObject *parent)
    : QObject(parent)
{

}

void FFMPEGWORKER::setParams(const QString & command, const QStringList & args )
{
    _command = command;
    _args = args;
}

void FFMPEGWORKER::process()
{
    QProcess theProcess;
    theProcess.start( _command, _args );
    theProcess.waitForFinished();
    emit finished();
}
