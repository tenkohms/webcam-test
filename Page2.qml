import QtQuick 2.0
import QtQuick.Controls 2.2
import Millipore 1.0
Item {

    property string thumbName: ""

    Rectangle {
        id: photoViewerBG
        anchors.fill: parent
        color: bgColor
        visible: true
    }

    BTPrinter {
        id: btPrinter
        onFinishedPrintJob: takeBG.visible = false
    }

    GridView {
        clip: true
        cellHeight: 125
        cellWidth: 150
        anchors.fill: photoViewerBG
        model: fileLister.files
        delegate: Image {
            height: 120
            width: 140
            fillMode: Image.PreserveAspectFit
            source: modelData
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log( modelData )
                    thumbName = modelData
                    previ.source = fileLister.getNonThumbName( modelData )
                    previ.visible = true
                }
            }
        }
    }

    Image {
        id: previ
        anchors.fill: photoViewerBG
        visible: false
        MouseArea {
            anchors.fill: previ
            onClicked: {
                if ( contextMenu.visible === false )
                {
                    contextMenu.x = mouseX
                    contextMenu.y = mouseY
                    contextMenu.visible = true
                }
                else {
                    contextMenu.visible = false
                }
            }
        }
    }

    Rectangle {
        radius: 8
        clip: true
        id: contextMenu
        visible: false

        color: "lightgrey"
        height: parent.height / 4
        width: parent.width / 4
        Rectangle {
            id: printButton
            radius: 8
            height: parent.height / 3
            width: parent.width
            anchors {
                left: parent.left
                top: parent.top
            }
            color: "lightgrey"
            Text {
                id: printText
                anchors.centerIn: parent
                color: "black"
                text: "Print"

            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    printButton.color = "dodgerblue"
                    printText.color = "white"
                }
                onReleased: {
                    printButton.color = "lightgrey"
                    printText.color = "black"
                }
                onClicked: {
                    takeBG.visible = true
                    btPrinter.print(previ.source)
                    contextMenu.visible = false
                }
            }
        }
        Rectangle {
            id: deleteButton
            radius: 8
            height: parent.height / 3
            width: parent.width
            anchors {
                left: parent.left
                top: printButton.bottom
            }
            color: "lightgrey"
            Text {
                id: deleteText
                anchors.centerIn: parent
                color: "black"
                text: "Delete"

            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    deleteButton.color = "dodgerblue"
                    deleteText.color = "white"
                }
                onReleased: {
                    deleteButton.color = "lightgrey"
                    deleteText.color = "black"
                }
                onClicked: {
                    deleteHandler.deleteFile( previ.source )
                    deleteHandler.deleteFile( thumbName )
                    thumbName = ""
                    contextMenu.visible = false
                    previ.source = "";
                    previ.visible = false;
                    fileLister.refresh();
                }
            }
        }
        Rectangle {
            id: closeButton
            radius: 8
            height: parent.height / 3
            width: parent.width
            anchors {
                left: parent.left
                top: deleteButton.bottom
            }
            color: "lightgrey"
            Text {
                id: closeButtonText
                anchors.centerIn: parent
                color: "black"
                text: "Close"

            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    closeButton.color = "dodgerblue"
                    closeButtonText.color = "white"
                }
                onReleased: {
                    closeButton.color = "lightgrey"
                    closeButtonText.color = "black"
                }
                onClicked: {
                    thumbName = ""
                    contextMenu.visible = false
                    previ.source = "";
                    previ.visible = false;
                    fileLister.refresh();
                }
            }
        }


    }

    Rectangle {
        id: takeBG
        visible: false
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, .8 )
        MouseArea {
            anchors.fill: parent
        }
    }

    Rectangle {
        id: processingPopup
        visible: takeBG.visible
        anchors.centerIn: takeBG
        radius: 8
        color: "lightgrey"
        height: parent.height / 2
        width: parent.width / 2
        Text {
            anchors.bottom: bIndicator.top
            anchors.bottomMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Sending print..."
            font.pixelSize: 20
        }

        BusyIndicator{
            id: bIndicator
            running: true
            anchors.centerIn: parent
        }
    }

}
