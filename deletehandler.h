#ifndef DELETEHANDLER_H
#define DELETEHANDLER_H

#include <QObject>

class DeleteHandler : public QObject
{
    Q_OBJECT
public:
    explicit DeleteHandler(QObject *parent = nullptr);
    Q_INVOKABLE void deleteFile( const QString & fileName );
    Q_INVOKABLE void deleteAllFiles();

signals:

public slots:
};

#endif // DELETEHANDLER_H
