#include "btprinter.h"
#include <QtDebug>

BTPrinter::BTPrinter(QObject *parent) : QObject(parent)
{
    worker = new BTWorker;
    mThread = new QThread;
    worker->moveToThread( mThread );

    connect( mThread, &QThread::started, worker, &BTWorker::process);
    connect( worker, &BTWorker::finished, this, &BTPrinter::processFinished);
}

BTPrinter::~BTPrinter()
{
    delete worker;
    delete mThread;
}

void BTPrinter::print(const QString & fileName )
{
    QString newFile = fileName;
    newFile = newFile.replace("file://", "");
    QString printCommand = "/usr/bin/obexftp --nopath --noconn --uuid none --bluetooth 70:2C:1F:29:36:26 --channel 4 -p " + newFile;
    QStringList arguments;
    //arguments << "--nopath" << "--noconn" << "--uuid none" << "--bluetooth 70:2C:1F:29:36:26" << "--channel 4" << "-p " + newFile;
    worker->setParams( printCommand, arguments );
    mThread->start();
    qDebug() << newFile;
}

void BTPrinter::processFinished()
{
    mThread->quit();
    emit finishedPrintJob();
}
