#ifndef BTWORKER_H
#define BTWORKER_H

#include <QObject>
#include <QProcess>

class BTWorker : public QObject
{
    Q_OBJECT
public:
    explicit BTWorker(QObject *parent = nullptr);
    void setParams( const QString & command, const QStringList & args );
signals:
    void finished();

public slots:
    void process();

private:
    QString _command;
    QStringList _args;
};

#endif // BTWORKER_H
