import QtQuick 2.0
import QtMultimedia 5.8
import Millipore 1.0

Item {

    property string bgColor: "#005ca9"
    property int startX: 0
    property int startY: 0
    property int finishX: 0
    property int finishY: 0
    property string pbImage
    property int counter: 0
    property string lastFile: ""
    property bool isInit: true

    property string currstate: "init"

    Component.onCompleted: {
        console.log( camera.availability )
        console.log( camera.cameraState )
        console.log( camera.cameraStatus )
    }

    FFMPEG {
        id: ffmpeg
    }

    Timer {
        id: initTimer
        running: false
        repeat: false
        interval: 500
        onTriggered: {
            //initScreen.visible = false
            console.log( "Triggered: " + currstate)
            if ( camera.cameraStatus != 8 )
            {
                console.log( "Camera: " + Camera.availability )
                initTimer.start()
            }
            else if ( currstate === "init" )
            {
                camera.imageCapture.captureToLocation("/tmp/")
                currstate = "postInit"
                initTimer.interval = 1000
                initTimer.start()
            }
            else if ( currstate === "postInit" )
            {
                photoPreviewFrame.visible = false
                initScreen.visible = false
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: bgColor
    }

    Camera {
        id: camera

        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
        captureMode: Camera.CaptureStillImage
        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }

        flash.mode: Camera.FlashRedEyeReduction
        imageCapture {
            onImageCaptured: {
                photoPreviewFrame.source = preview
            }
            onImageSaved: {
                console.log( path )
                lastFile = path
                if ( lastFile.indexOf("tmp") !== -1 ) {
                    photoPreviewFrame.visible = true
                    photoPreviewFrame.visible = false
                }
                else
                {
                    photoPreviewFrame.visible = true
                }
            }
        }
    }

    VideoOutput {
        id: vout
        source: camera
        fillMode: Image.PreserveAspectCrop
        anchors {
            fill: parent
        }

        focus : visible // to receive focus and capture key events when visible
    }

    MouseArea {
        anchors.fill: vout
        onClicked: {
            if ( photoPreviewFrame.visible === false )
            {
                if ( isDesktop )
                {
                    camera.imageCapture.captureToLocation("/home/hpham/Desktop/images/")
                }
                else
                {
                    camera.imageCapture.captureToLocation("/home/pi/images/")
                }
            }
        }
    }

    Image {
        id: photoPreviewFrame
        anchors.fill: parent
        visible: true
        onVisibleChanged: {
            if ( !visible )
                source = ""
        }
    }

    Image {
        id: saveButton
        visible: photoPreviewFrame.visible
        anchors {
            top: parent.top
            right: parent.right
            margins: 20
        }
        width: parent.width / 10
        fillMode: Image.PreserveAspectFit
        source: "Save.png"
    }

    MouseArea {
        anchors.fill: saveButton
        onPressed: saveButton.source = "SavePressed.png"
        onReleased: saveButton.source = "Save.png"
        onClicked: {
            ffmpeg.createThumb( lastFile )
            photoPreviewFrame.visible = false
        }
    }

    Image {
        id: deleteButton
        visible: photoPreviewFrame.visible
        anchors {
            top: saveButton.bottom
            right: parent.right
            margins: 20
        }
        width: parent.width / 10
        fillMode: Image.PreserveAspectFit
        source: "Trash.png"
    }

    MouseArea {
        anchors.fill: deleteButton
        onPressed: deleteButton.source = "TrashPressed.png"
        onReleased: deleteButton.source = "Trash.png"
        onClicked: {
            photoPreviewFrame.visible = false
            deleteHandler.deleteFile( lastFile )
        }
    }

    ZoomControl {
        x : 0
        y : 0
        width : 100
        height: parent.height
        visible: ( !photoPreviewFrame.visible)

        currentZoom: camera.digitalZoom
        maximumZoom: Math.min(4.0, camera.maximumDigitalZoom)
        onZoomTo: camera.setDigitalZoom(value)
    }

    Rectangle {
        id: initScreen
        anchors.fill: parent
        Component.onCompleted: initTimer.start()
        color: bgColor
        Text {
            anchors.centerIn: parent
            font.pixelSize: 22
            anchors.horizontalCenter: Text.horizontalCenter
            color: "white"
            text: "Loading..."
        }
    }
}
