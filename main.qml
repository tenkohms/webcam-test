import QtQuick 2.9
import QtQuick.Window 2.2
import QtMultimedia 5.8
import Millipore 1.0
import QtQuick.Controls 2.2

Window {
    id: root
    property bool isDesktop: false
    property string bgColor: "#005ca9"
    property string messageText: ""
    visible: true
    width: 800
    height: 480

    property string currstate: "page1"

    Deleter {
        id: deleteHandler
    }

    FLister {
        id: fileLister
        Component.onCompleted: {
            fileLister.setDesktop(root.isDesktop)
        }
    }


    Timer {
        id: dateTimer
        interval: 100
        onTriggered: {

            var date = new Date()
            var dateString = date.toTimeString()
            timeText.text = dateString
        }
        running: true
        repeat: true
    }

    Timer {
        id: splashTimer
        interval: 5000
        running: false
        repeat: false
        onTriggered: {
            splashBG.visible = false
        }
    }

    Component.onCompleted: splashTimer.start()

    Rectangle {
        anchors.fill: parent
        color: bgColor
    }

    Rectangle {
        id: topHeader
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        height: parent.height / 10
        color: bgColor
    }

    Text {
        id: timeText
        anchors {
            centerIn: topHeader
        }
        color: "white"
        text: "12:46:55"
        font.pixelSize: 18
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    Image {
        id: logo
        anchors {
            top: topHeader.top
            leftMargin: 10
            verticalCenter: topHeader.verticalCenter
            topMargin: 10
            left: topHeader.left
        }
        height: topHeader.height * .8
        fillMode: Image.PreserveAspectFit
        source: "LOGO.png"

    }
    MouseArea {
        anchors.fill: logo
        onClicked: {
            drawer.visible = !drawer.visible
//            if ( currstate === "page1" ) {
//                fileLister.refresh()
//                p2.visible = true
//                p1.visible = false
//                currstate = "page2"
//            }
//            else {
//                p2.visible = false
//                p1.visible = true
//                currstate = "page1"
//            }
        }
    }

    Image {
        id: battery
        anchors {
            top: topHeader.top
            rightMargin: 10
            verticalCenter: topHeader.verticalCenter
            topMargin: 10
            right: topHeader.right
        }
        height: topHeader.height * .8
        fillMode: Image.PreserveAspectFit
        source: "battery.png"
    }

    Image {
        id: wifi
        anchors {
            top: topHeader.top
            rightMargin: 10
            verticalCenter: topHeader.verticalCenter
            topMargin: 10
            right: battery.left
        }
        height: topHeader.height * .8
        fillMode: Image.PreserveAspectFit
        source: "Wifi.png"
    }

    Image {
        id: bt
        anchors {
            top: topHeader.top
            rightMargin: 10
            verticalCenter: topHeader.verticalCenter
            topMargin: 10
            right: wifi.left
        }
        height: topHeader.height * .8
        fillMode: Image.PreserveAspectFit
        source: "Bluetooth.png"
    }

    Page1 {
        id: p1
        visible: true
        anchors {
            top: topHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    Page2 {
        id: p2
        visible: false
        anchors {
            top: topHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    Drawer {
        id: drawer
        y: topHeader.height
        width: parent.width * 0.6
        height: parent.height - topHeader.height
        Rectangle {
            id: drawerCameraBG
            width: parent.width * .85
            height: parent.height / 8
            color: ( root.currstate === "page1" ? bgColor : "white" )
            radius: 5
            anchors {
                top: parent.top
                horizontalCenter: parent.horizontalCenter
                topMargin: 15
            }
            Text {
                id: drawerCameraText
                text: "Camera"
                color: ( root.currstate === "page1" ? "white" : "black" )
                anchors.centerIn: parent
                font.pixelSize: 20
            }
            MouseArea {
                anchors.fill: parent
                enabled: ( root.currstate === "page1" ? false : true )
                onPressed: {
                    drawerCameraBG.color = bgColor
                    drawerCameraText.color = "white"
                }
                onReleased: {
                    drawerCameraBG.color = "white"
                    drawerCameraText.color = "black"
                }
                onClicked: {
                    root.currstate = "page1"
                    drawerCameraBG.color = bgColor
                    drawerCameraText.color = "white"
                    drawerImagesButton.color = "white"
                    drawerImageText.color = "black"
                    p1.visible = true
                    p2.visible = false
                    drawer.visible = false
                }
            }
        }
        Rectangle {
            id: drawerImagesButton
            width: parent.width * .85
            height: parent.height / 8
            color: ( root.currstate === "page2" ? bgColor : "white" )
            radius: 5
            anchors {
                top: drawerCameraBG.bottom
                horizontalCenter: parent.horizontalCenter
                topMargin: 15
            }
            Text {
                id: drawerImageText
                text: "Images"
                color: ( root.currstate === "page2" ? "white" : "black" )
                anchors.centerIn: parent
                font.pixelSize: 20
            }
            MouseArea {
                anchors.fill: parent
                enabled: ( root.currstate === "page2" ? false : true )
                onPressed: {
                    drawerImagesButton.color = bgColor
                    drawerImageText.color = "white"
                }
                onReleased: {
                    drawerImagesButton.color = "white"
                    drawerImageText.color = "black"
                }
                onClicked: {
                    root.currstate = "page2"
                    drawerImagesButton.color = bgColor
                    drawerImageText.color = "white"
                    drawerCameraBG.color = "white"
                    drawerCameraText.color = "black"
                    p1.visible = false
                    p2.visible = true
                    drawer.visible = false
                }
            }
        }
        Rectangle {
            id: drawerSettingsButton
            width: parent.width * .85
            height: parent.height / 8
            color: "white"
            radius: 5
            anchors {
                top: drawerImagesButton.bottom
                horizontalCenter: parent.horizontalCenter
                topMargin: 15
            }
            Text {
                id: drawerSettingsText
                text: "Settings"
                color: "black"
                anchors.centerIn: parent
                font.pixelSize: 20
            }
        }

    }


    Image {
        id: splashBG
        anchors.fill: parent
        visible: true
        source: "splash.png"
        fillMode: Image.PreserveAspectFit
    }

}
