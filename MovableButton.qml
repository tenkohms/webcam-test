import QtQuick 2.0

Image {
    function moveMe() {
        cameraButton.x = cameraButton.x + ( finishX - startX )
        cameraButton.y = cameraButton.y + ( finishY - startY )

        if ( cameraButton.x + ( finishX - startX ) < 0 )
        {
            cameraButton.x = 0
        }

        if ( cameraButton.x + ( finishX - startX ) + cameraButton.width > mainWindow.width )
        {
            cameraButton.x = mainWindow.width - cameraButton.width
        }

        if ( cameraButton.y + ( finishY - startY ) < 0 )
        {
            cameraButton.y = 0;
        }

        if ( cameraButton.y + ( finishY - startY ) + cameraButton.height > mainWindow.height )
        {
            cameraButton.y = mainWindow.height - cameraButton.height
        }

    }

    id: cameraButton
    x: parent.width / 2 - width/2
    y: parent.height - height
    width: parent.width / 10
    fillMode: Image.PreserveAspectFit
    source: "CameraButton.png"
    MouseArea {
        property bool moveActivated: false
        anchors.fill: parent

        onPositionChanged: {
            if ( moveActivated ) {
                finishX = mouseX
                finishY = mouseY
                cameraButton.moveMe()
            }
        }

        onPressAndHold: {
            startX = mouseX
            startY = mouseY
            parent.source = "CameraPressed.png"
            moveActivated = true
        }

        onPressed: {
            parent.source = "CameraMove.png"
        }

        onReleased: {
            if ( !moveActivated )
            {
                camera.imageCapture.capture()
            }

            parent.source = "CameraButton.png"
            moveActivated = false
        }
    }
}
