#ifndef FFMPEGGER_H
#define FFMPEGGER_H

#include <QObject>
#include <QThread>
#include "ffmpegworker.h"

class FFMPEGGER : public QObject
{
    Q_OBJECT
public:
    explicit FFMPEGGER(QObject *parent = nullptr);
    ~FFMPEGGER();
    Q_INVOKABLE void createThumb( const QString & fileName );

signals:

public slots:
    void processFinished();

private:
    QThread * mThread;
    FFMPEGWORKER * worker;
};

#endif // FFMPEGGER_H
