#include "ffmpegger.h"
#include <QDebug>
FFMPEGGER::FFMPEGGER(QObject *parent) : QObject(parent)
{
    worker = new FFMPEGWORKER;
    mThread = new QThread;
    worker->moveToThread(mThread);

    connect( mThread, &QThread::started, worker, &FFMPEGWORKER::process);
    connect( worker, &FFMPEGWORKER::finished, this, &FFMPEGGER::processFinished );
}

FFMPEGGER::~FFMPEGGER()
{
    delete worker;
    delete mThread;
}

void FFMPEGGER::createThumb(const QString & fileName )
{
    qDebug() << "Creating process";
    QString ffmpegCommand = "/usr/bin/ffmpeg";
    QString newFile = fileName;
    newFile.replace("/images/", "/images/thumbs/");
    QStringList arguments;
    arguments << "-i" << fileName << "-vf" <<
                 "scale=120:-1" << newFile;

    //qDebug() << ffmpegCommand << arguments;
    worker->setParams( ffmpegCommand, arguments );
    mThread->start();
}

void FFMPEGGER::processFinished()
{
    qDebug() << "stopping thread";
    mThread->quit();
}
