#include "btworker.h"
#include <QDebug>

BTWorker::BTWorker(QObject *parent) : QObject(parent)
{

}

void BTWorker::setParams( const QString & command, const QStringList & args )
{
    _command = command;
    _args = args;
    qDebug() << command;
}

void BTWorker::process()
{
    QProcess theProcess;
    theProcess.start( _command );
    theProcess.waitForFinished();
    emit finished();
}
