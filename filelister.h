#ifndef FILELISTER_H
#define FILELISTER_H

#include <QObject>

class FileLister : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList files READ files WRITE setFiles NOTIFY filesChanged)
public:
    explicit FileLister(QObject *parent = nullptr);
    QStringList files();
    void setFiles( QStringList nFiles );
    Q_INVOKABLE void setDesktop( bool isDesktop );
    Q_INVOKABLE void refresh();
    Q_INVOKABLE QString getNonThumbName( const QString & thumbName );
signals:
    void filesChanged( QStringList nFiles );

public slots:

private:
    QStringList _files;
    bool isDesktop_;
};

#endif // FILELISTER_H
