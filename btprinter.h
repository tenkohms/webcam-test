#ifndef BTPRINTER_H
#define BTPRINTER_H

#include <QObject>
#include <QThread>
#include "btworker.h"

class BTPrinter : public QObject
{
    Q_OBJECT
public:
    explicit BTPrinter(QObject *parent = nullptr);
    ~BTPrinter();
    Q_INVOKABLE void print( const QString & fileName );

signals:
    void finishedPrintJob();

public slots:
    void processFinished();
private:
    QThread * mThread;
    BTWorker * worker;

};

#endif // BTPRINTER_H
